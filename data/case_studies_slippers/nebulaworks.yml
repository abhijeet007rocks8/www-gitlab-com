title: Nebulaworks
file_name: nebulaworks
canonical_path: /customers/nebulaworks/
cover_image: /images/blogimages/nebulaworks.jpg
cover_title: |
  How Nebulaworks replaced 3 tools with GitLab and empowered customer speed and agility 
cover_description: |
  Nebulaworks, an engineering consultancy organization, adopted GitLab for SCM, CI/CD, and issue tracking, and increased connection with the marketing team in the process.
twitter_image: /images/blogimages/nebulaworks.jpg
twitter_text: Learn how @nebulaworks adopted GitLab for SCM, CI/CD, and issue tracking, and increased collaboration with the marketing team.
customer_logo: /images/logos/nebulaworks-logo-blue.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Irvine, CA
customer_solution: GitLab Enterprise
customer_employees: 15 
customer_overview: |
  Nebulaworks adopted GitLab, eased tool maintenance and overhead, and achieved CI organization.  
customer_challenge: |
  Nebulaworks spent too much time maintaining a toolchain consisting of disparate tools and needed a single platform to focus on customer success.
key_benefits: >-

  
    Superior CI integration 

  
    Eliminated multi-toolchain

  
    Zero context switching

  
    End-to-end visibility

  
    Simplified workflow

  
    Enhanced collaboration
customer_stats:
  - stat: 1  
    label: Tool replaced 3 
  - stat: 2    
    label: Week production release time 
  - stat: 20     
    label: Points per sprint 
customer_study_content:
  - title: the customer
    subtitle: Consultancy by engineers for engineers
    content: >-
    
  
        Nebulaworks is a software engineering consultancy firm that provides customers with innovative solutions for development and delivery processes. The organization prides itself around work with its customers to create high-performing engineering teams where members are inspired to collaborate openly, incentivized to gather new knowledge and skills, and are fulfilled by solving complex problems simply. 
    
    
        <a href="https://www.nebulaworks.com/about" target="_blank">Nebulaworks</a> was founded in 2014 by two engineers who saw a need to challenge the status quo of software development and IT Ops services delivery in large enterprises. Different from many of the consulting firms and Global SI’s of the time, Nebulaworks was built to solve the complex challenges of the enterprise IT engineer. A consulting and SI firm, built for engineers by engineers.
      
  - title: the challenge
    subtitle: Three tools too many
    content: >-
    
  
        Nebulaworks was looking for a platform that provided remote repositories to empower teams to collaborate — regardless of location. The development team was previously using a self managed instance of a git repository and a separate issue board software for issues and tracking. They wanted to increase productivity and focus their engineering efforts on development that would impact the business, rather than dealing with the daily administrative tasks just to keep the system online.  
    
  
        The organization had self-hosted continuous integration service that was backed by Kubernetes. This was not an ideal solution, due to the administration overhead and caused more work for the engineers who were consuming the system. 
    
   
        Nebulaworks was maintaining a total of three internal tools for the course of several years. It was a full-time job for an engineer to manage and maintain the tools, which reduced time for software engineering. On top of that, having data and user permissions in various places caused a lot of context switching, which was time-consuming and inefficient.
  - title: the solution
    subtitle: One platform, many functionalities
    content: >-
    
  
        Before renewing the license for the existing three internal, self-hosted tools, Rob Hernandez, Chief Technology Officer, and his team researched other platforms. When they demoed GitLab, they mirrored an existing project, fit it for the CI portion to test, and then wrapped all the issue tracking and board structures. Hernandez found that GitLab’s level of organization and the ability to provide a hierarchy of different projects stood out against competitors. 
    
  
        “Realizing that we could even have all the issues rolled up to the top-level GitLab group which was really cool. We wouldn't be able to do that with our existing self managed git service,” Hernandez said. “Going through the tool in the demo, that was great. And realizing that with the hierarchy we could have subgroups, and we could break those subgroups into how we organize projects for a given customer.”
    
  
        GitLab offered the team a singular platform for CI integration, code management, collaboration, and issue tracking without the need to layer any tools. Nebulaworks is able to provide customers with a collaborative and transparent experience.  A focus on a transparent relationship reduces cost for everyone by enabling faster resolution of issues, and reduces risk by creating trust and enabling both sides to plan and execute accordingly. With GitLab, Nebulaworks was able to truly focus on deliverables instead of performing updates and toolchain maintenance. 

  - title: the results
    subtitle: CI, code management, and customer success 
    content: >-
    
  
        GitLab breaks down silos as a centralized platform for collaboration, helping to drive the company forward. The team now has a simplified workflow, including issues that are close to the code, end-to-end visibility, easily integrated CI, and no more context switching between tools.  
    
  
        Nebulaworks fully replaced its internal, self-hosted Git stack with GitLab. “We went as far as to define all of our resources in GitLab (repositories, groups, permissions, etc.) using Terraform. That way, GitLab gets changed just like any other piece of code -- submit a MR, apply it and merge it,” Hernandez said. “It’s really cool to see new hires add their permissions on the first day via a MR and that’s the way it should be. There’s no other way for someone to make a change within our GitLab Nebulaworks group.”  
    
  
        Nebulaworks selected GitLab Gold, because the SaaS capabilities allowed the team to shut down some on-prem machines and gain the benefits of a hosted offering. GitLab is powering their [deployments across AWS](/blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab/), specifically their container workloads running on top of Amazon EKS clusters.  
    
  
        By moving to SaaS, the team is able to optimize its efficiencies by leveraging the GitLab infrastructure, and to focus on delivering better products to customers. “We're not worrying about security patches or upgrading to new versions for new features. All of those things are taken care of by GitLab,” Hernandez said. “Now we are focusing on enabling our engineering team as a whole, across all the services and functionality that we need. Gitlab allows us to focus on that instead of focusing on maintenance.”
    
  
        Because the engineering team works with many different tools with different clients, they need to focus on the statement of work. The team measures success against what gets delivered and the time it takes to deliver, which requires a dependable tool that can work with a variety of other tools. “With GitLab, we release every two weeks to production. That's a business need. That's how we want to do it. It's easy for us. It's low stress. We correctly test things, let them bake in the development and staging capacity, before they get out to production,” Hernandez said.
    
  
        The engineering team at Nebulaworks is not the only one using GitLab. To help improve coordination between marketing and the engineering group, the content marketing team [collaborates in GitLab](/topics/version-control/software-team-collaboration/). The company had planned on creating content for the engineering consultancy and GitLab provided a simple way to work closely with the engineering team to create quality content. Both teams use GitLab issues and boards to communicate and content is added to the website in merge requests.
    
  
        "When we decided to invest in content marketing, we knew we had to figure out a solution that would allow marketing and engineering to work seamlessly together. The simplicity of GitLab's features made that possible for us,” said Anne Lin, Marketing and Brand Manager. “The marketing team quickly adopted the engineering team's workflow using issue tracking, kanboards, and merge requests to collaborate on content production. By leveraging the same workflow, the two teams were able to generate trust and visibility into each other's work."
    
  
        Using GitLab means that the teams can work asynchronously. Working from home is optional at Nebulaworks. As the company has adopted the work from home lifestyle, they’ve been able to collaborate easily. “We have not missed a beat. How we collaborate with our customers, how we work with our customers, how we work on projects, that workflow has not changed,” said Patrick Collins, VP, Sales and Customer Success. “It's been a huge success having this process in place, going to a large group coming into the office, now 100% remote.”
    
  
        ## Learn more about GitLab solutions
    
  
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
    
  
        [Optimize your DevOps value stream](/solutions/value-stream-management/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
customer_study_quotes:
  - blockquote: When we adopted GitLab, we went all in, because it simplified a lot of the day-to-day maintenance. We don't have a lot of time to deal with the platform where we're storing our code,” Hernandez said. “It frees us up to do things that are either internal to the engineering team or to focus on customer interactions. 
    attribution: Rob Hernandez  
    attribution_title: Chief Technology Officer, Nebulaworks  

