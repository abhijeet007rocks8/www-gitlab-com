---
layout: markdown_page
title: "Omar Fernandez's Priorities"
---

Inspired by [Kenny Johnston's Priorities page](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md)

## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis.

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not completed

## 2021-10-04
1. Y - Conduct GitLab advocates interviews [CoST #148](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/148)
1. Coordinate video editing and proper releases for those to be used in upcoming events [CoST #148](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/148)

## 2021-09-27
1. Conduct GitLab advocates interviews [CoST #148](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/148)
1. Coordinate video editing and proper releases for those to be used in upcoming events [CoST #148](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/148)
1. Y - Coffee chats [2]

## 2021-09-20
1. Y - Work on Internal Presentation [CoST #145](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/145)
1. Start work on GitLab advocates interviews [CoST #148](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/148)
1. Y - Coffee chats [4]

## 2021-09-13
1. Y - Coffee chats [3]
1. Work on Internal Presentation [CoST #145](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/145)

## 2021-09-07
Note - OOO 2021-09-06 for Labor Day
1. GitLab Onboarding issue
1. Setting up accounts
1. Y - Coffee chats to get to know the team [8 coffee chats!]
1. Internal presentation
